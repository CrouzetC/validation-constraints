package net.tncy.ccr.validator;

import javax.validation.ValidatorFactory;
import javax.validation.Validator;
import javax.validation.Validation;
import javax.validation.ConstraintViolation;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;

/**
 * Testing the annotation @ISBN
 */
public class ISBNValidatorTest {

    private static ValidatorFactory factory;

    @BeforeClass
    public static void initClass() {
        factory = Validation.buildDefaultValidatorFactory();
    }

    // Test 10 chars
    @Test
    public void test1() {
        TestISBNBean bean = new TestISBNBean("1234567890");
	Validator validator = factory.getValidator();
	Set<ConstraintViolation<TestISBNBean>> constraintViolations = validator.validate(bean);
	assertEquals(0, constraintViolations.size());
    }

    // Test 13 chars
    @Test
    public void test2() {
        TestISBNBean bean = new TestISBNBean("1234567890123");
	Set<ConstraintViolation<TestISBNBean>> constraintViolations = factory.getValidator().validate(bean);
	assertEquals(0, constraintViolations.size());
    }

    // Test 9 chars
    @Test
    public void test3() {
        TestISBNBean bean = new TestISBNBean("123456789");
	assertEquals(1, factory.getValidator().validate(bean).size());
    }

    // Test 1 char
    @Test
    public void test4() {
        TestISBNBean bean = new TestISBNBean("a");
	assertEquals(1, factory.getValidator().validate(bean).size());
    }

    // Test 0 char
    @Test
    public void test5() {
        TestISBNBean bean = new TestISBNBean("");
	assertEquals(1, factory.getValidator().validate(bean).size());
    }

    // Test null
    @Test
    public void test6() {
        TestISBNBean bean = new TestISBNBean(null);
        assertEquals(1, factory.getValidator().validate(bean).size());
    }

    // Test 14 chars
    @Test
    public void test7() {
        TestISBNBean bean = new TestISBNBean("7777"+"7777"+"7777"+"77");
	assertEquals(1, factory.getValidator().validate(bean).size());
    }

}
