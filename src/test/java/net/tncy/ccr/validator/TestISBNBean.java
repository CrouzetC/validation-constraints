package net.tncy.ccr.validator;

/**
 * Test class containing only an ISBN String
 */
public class TestISBNBean {

    @ISBN
    private String isbn;

    public TestISBNBean(String isbn) {
        this.isbn = isbn;
    }

}
