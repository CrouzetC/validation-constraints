package net.tncy.ccr.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {
    
    @Override
    public void initialize(ISBN constraintAnnotation) {
        // ici on peut accéder aux attribut de l'annotation
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {

        if (bookNumber == null)
            return false;

        // Checking that the ISBN has between 10 and 13 characters
        boolean valid = bookNumber.length() >= 10 && bookNumber.length() <= 13;

        return valid;
    }

}
